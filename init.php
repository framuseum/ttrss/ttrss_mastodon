<?php
class Mastodon extends Plugin {
	private $host;

	function init($host) {
		$this->host = $host;

		$host->add_hook($host::HOOK_ARTICLE_BUTTON, $this);
	}

	function about() {
		return array(1.0,
			"Share article on Mastodon",
			"framasky",
			false);
	}

	function get_js() {
		return file_get_contents(dirname(__FILE__) . "/mastodon.js");
	}

	function hook_article_button($line) {
		$article_id = $line["id"];

		$rv = "<img src=\"plugins.local/mastodon/mastodon.ico\"
			class='tagsPic' style=\"cursor: pointer; height: 16px;\"
			onclick=\"mastodonArticle($article_id)\"
			title='".__('Share on Mastodon')."'>";

		return $rv;
	}

	function getInfo() {
		$sth = $this->pdo->prepare("SELECT title, link
				FROM ttrss_entries, ttrss_user_entries
				WHERE id = ? AND ref_id = id AND owner_uid = ?");
		$sth->execute([$_REQUEST['id'], $_SESSION['uid']]);

		if ($sth->rowCount() != 0) {
			$row = $sth->fetch();
			$title = truncate_string(strip_tags($row['title']),
				400, '...');
			$article_link = $row['link'];
		}

		print json_encode(array("title" => $title, "link" => $article_link, "id" => $id));
	}

	function api_version() {
		return 2;
	}

}
?>
