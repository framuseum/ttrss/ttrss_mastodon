[![](https://framagit.org/assets/favicon-075eba76312e8421991a0c1f89a89ee81678bcde72319dd3e8047e2a47cd3a42.ico)](https://framagit.org)

![English:](https://upload.wikimedia.org/wikipedia/commons/thumb/a/ae/Flag_of_the_United_Kingdom.svg/20px-Flag_of_the_United_Kingdom.svg.png) **Framasoft uses GitLab** for the development of its free softwares. Our Github repositories are only mirrors.
If you want to work with us, **fork us on [framagit.org](https://framagit.org)**. (no registration needed, you can sign in with your Github account)

![Français :](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Flag_of_France.svg/20px-Flag_of_France.svg.png) **Framasoft utilise GitLab** pour le développement de ses logiciels libres. Nos dépôts Github ne sont que des miroirs.
Si vous souhaitez travailler avec nous, **forkez-nous sur [framagit.org](https://framagit.org)**. (l'inscription n'est pas nécessaire, vous pouvez vous connecter avec votre compte Github)
* * *

# Mastodon

Mastodon plugin for Framanews (Tiny Tiny RSS)
https://framagit.org/framasoft/ttrss/ttrss_mastodon

Licensed under GNU GPL version 3

Copyright (c) 2018 Framasoft

## Requirements

* tt-rss working from trunk at:
https://git.tt-rss.org/git/tt-rss

## Install
Go to your ttrss dir and:
```shell
cd plugins.local
git clone https://framagit.org/framasoft/ttrss/ttrss_mastodon.git mastodon
```

Enable plugin from your preferences or in `config.php`.

## Acknowledgments

This plugin is heavily based on Fox's tweet plugin
